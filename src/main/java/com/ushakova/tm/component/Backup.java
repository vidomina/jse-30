package com.ushakova.tm.component;

import com.ushakova.tm.bootstrap.Bootstrap;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

public class Backup extends Thread {

    private static final int INTERVAL = 30000;

    private static final String BACKUP_SAVE = "bkp-save";

    private static final String BACKUP_LOAD = "bkp-load";

    @NotNull
    final Bootstrap bootstrap;

    public Backup(@NotNull Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        this.setDaemon(true);
    }

    public void init() {
        load();
        start();
    }

    public void load() {
        bootstrap.parseCommand(BACKUP_LOAD);
    }

    @Override
    @SneakyThrows
    public void run() {
        while (true) {
            save();
            Thread.sleep(INTERVAL);
        }
    }

    public void save() {
        bootstrap.parseCommand(BACKUP_SAVE);
    }

}