package com.ushakova.tm.service;

import com.ushakova.tm.api.repository.IBusinessRepository;
import com.ushakova.tm.api.service.IBusinessService;
import com.ushakova.tm.enumerated.Status;
import com.ushakova.tm.exception.empty.EmptyIdException;
import com.ushakova.tm.exception.empty.EmptyNameException;
import com.ushakova.tm.exception.entity.EntityNotFoundException;
import com.ushakova.tm.exception.system.IncorrectIndexException;
import com.ushakova.tm.model.AbstractBusinessEntity;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Comparator;
import java.util.List;

public abstract class AbstractBusinessService<E extends AbstractBusinessEntity> extends AbstractService<E> implements IBusinessService<E> {

    @NotNull
    protected IBusinessRepository<E> repository;

    public AbstractBusinessService(@NotNull IBusinessRepository<E> repository) {
        super(repository);
        this.repository = repository;
    }

    @Override
    @NotNull
    public E changeStatusById(@Nullable final String id, @NotNull final Status status, @Nullable final String userId) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final E entity = findById(id, userId);
        entity.setStatus(status);
        return entity;
    }

    @Override
    @NotNull
    public E changeStatusByIndex(@Nullable final Integer index, @NotNull final Status status, @Nullable final String userId) {
        if (index == null || index < 0) throw new IncorrectIndexException();
        @NotNull final E entity = findByIndex(index, userId);
        entity.setStatus(status);
        return entity;
    }

    @Override
    @NotNull
    public E changeStatusByName(@Nullable final String name, @NotNull final Status status, @Nullable final String userId) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        @NotNull final E entity = findByName(name, userId);
        entity.setStatus(status);
        return entity;
    }

    @Override
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        repository.clear(userId);
    }

    @Override
    @NotNull
    public E completeById(final String id, final String userId) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        final E entity = findById(id);
        if (entity == null) throw new EntityNotFoundException();
        entity.setStatus(Status.COMPLETE);
        return entity;
    }

    @Override
    @NotNull
    public E completeByIndex(@Nullable final Integer index, @Nullable final String userId) {
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        @NotNull final E entity = findByIndex(index, userId);
        entity.setStatus(Status.COMPLETE);
        return entity;
    }

    @Override
    @NotNull
    public E completeByName(@Nullable final String name, @Nullable final String userId) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        @NotNull final E entity = findByName(name, userId);
        entity.setStatus(Status.COMPLETE);
        return entity;
    }

    @Override
    @Nullable
    public List<E> findAll(@Nullable Comparator<E> comparator, @Nullable final String userId) {
        if (comparator == null) return null;
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        return repository.findAll(comparator, userId);
    }

    @Override
    @NotNull
    public E findById(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final E entity = repository.findById(userId, id);
        if (entity == null) throw new EntityNotFoundException();
        return entity;
    }

    @Override
    @NotNull
    public E findByIndex(@Nullable final Integer index, @Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        @Nullable E entity = repository.findByIndex(index, userId);
        if (entity == null) throw new EntityNotFoundException();
        return entity;
    }

    @Override
    @NotNull
    public E findByName(@Nullable final String name, @Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable E entity = repository.findByName(name, userId);
        if (entity == null) throw new EntityNotFoundException();
        return entity;
    }

    @Override
    @Nullable
    public E removeById(@Nullable final String id, @Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.removeById(id, userId);

    }

    @Override
    @Nullable
    public E removeByIndex(@Nullable final Integer index, @Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        return repository.removeByIndex(index, userId);
    }

    @Override
    @Nullable
    public E removeByName(@Nullable final String name, @Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return repository.removeByName(name, userId);
    }

    @Override
    @NotNull
    public E startById(@Nullable final String id, @Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final E entity = findById(id, userId);
        entity.setStatus(Status.IN_PROGRESS);
        return entity;
    }

    @Override
    @NotNull
    public E startByIndex(@Nullable final Integer index, @Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        @NotNull final E entity = findByIndex(index, userId);
        entity.setStatus(Status.IN_PROGRESS);
        return entity;
    }

    @Override
    @NotNull
    public E startByName(@Nullable final String name, @Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final E entity = findByName(name, userId);
        entity.setStatus(Status.IN_PROGRESS);
        return entity;
    }

    @Override
    @NotNull
    public E updateById(@Nullable final String id, @Nullable final String name, @Nullable final String description, @Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final E entity = findById(id, userId);
        entity.setName(name);
        entity.setDescription(description);
        return entity;
    }

    @Override
    @NotNull
    public E updateByIndex(@Nullable final Integer index, @Nullable final String name, @Nullable final String description, @Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final E entity = findByIndex(index, userId);
        entity.setName(name);
        entity.setDescription(description);
        return entity;
    }

}
