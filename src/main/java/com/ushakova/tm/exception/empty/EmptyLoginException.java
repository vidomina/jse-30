package com.ushakova.tm.exception.empty;

import com.ushakova.tm.exception.AbstractException;

public class EmptyLoginException extends AbstractException {

    public EmptyLoginException() {
        super("An error has occurred: login is empty.");
    }

}
